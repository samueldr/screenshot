`screenshot`
============

A utility to take screenshots


Installation
------------

Assuming you want the code to reside in `$HOME/opt`, you keep your utilities
in `$HOME/.bin` and your shell is bash.

```
$ mkdir -p "$HOME/opt"
$ cd "$HOME/opt"
$ git clone https://gitlab.com/zr.OS/screenshot.git
$ cat <<'EOF' > "$HOME/.bin/screenshot"
#!/usr/bin/env bash
exec "$HOME/opt/screenshot" "$@"
EOF
$ chmod +x "$HOME/.bin/screenshot"
```

### Dependencies

This will require:

  * [`slop`](https://github.com/naelstrof/slop)
  * [`maim`](https://github.com/naelstrof/maim)

#### Arch Linux

They both are available in `community` for *Arch Linux*.

#### NixOS

  * `pkgs.slop`
  * `pkgs.maim`


Usage
-----

See `maim` for details with command line options.

  * `-s` Select a section.

I would highly recommend binding to a global key shortcut.


Configuration
-------------

This script will read a configuration in `$XDG_CONFIG_HOME/zr/screenshot/config`

See the `example.config` file in the repository for details on the configuration.


Issues
------

### Generic name

This program assumes the generic `screenshot` name. Where a distribution would
want to package the program and make it available, please rename it either
`zr.screenshot` or `zr-screenshot`, leaving the generic command `screenshot`
available for anything better suited for the user, and making the utility still
accessible.
